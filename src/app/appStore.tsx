import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import appReducer from 'src/app/reducers/appReducer';
import appSaga from 'src/app/sagas/appSaga';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

const composeEnhancer =
  (process.env.NODE_ENV !== 'production' &&
    // @ts-ignore
    window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']) ||
  compose;

export const appStore = createStore(
  appReducer,
  {},
  composeEnhancer(applyMiddleware(sagaMiddleware))
);

// then run the saga
sagaMiddleware.run(appSaga)

export default appStore;