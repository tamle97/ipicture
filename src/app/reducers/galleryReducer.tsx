import { AnyAction } from "redux";
import { galleryActionType, GET_GALLERY_SUCCESS, SET_LOADING_GALLERY } from "../actionTypes/galleryActionTypes";

const initialGalleryState = {
  isLoading: false,
  showedPhotos: [],
  allPhotos: [],
  peoplePhotos: [],
  placesPhotos: [],
  thingsPhotos: [],
};

export const galleryReducer = (
  state = initialGalleryState,
  action: galleryActionType
) => {
  switch (action.type) {
    case GET_GALLERY_SUCCESS: {
      return {
        ...state,
        allPhotos: action.photos,
        showedPhotos: action.photos,
        isLoading: false
      }
    }
    case SET_LOADING_GALLERY: {
      return {
        ...state,
        isLoading: action.isLoading
      }
    }
    default:
      return state;
  }
};