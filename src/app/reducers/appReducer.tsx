import { combineReducers } from 'redux';
import { galleryReducer } from './galleryReducer';
//Import some reducer to combine

const appReducer = combineReducers({
    galleryState: galleryReducer
})

export default appReducer;