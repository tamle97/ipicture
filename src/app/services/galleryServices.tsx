import { appAxios } from "src/api/appAxios";

export const galleryServices = {
    getGallery: async (topic: string, perPage: number) => {
        const data = appAxios.get(`v1/search?query=${topic}&per_page=${perPage}`)
            .then(res => {
                return res;
            })
        return data;
    }
}