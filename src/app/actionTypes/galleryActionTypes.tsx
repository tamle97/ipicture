import { IPhoto } from "../interfaces/IGallery";

export const GET_GALLERY = 'GET_GALLERY';
export const SET_LOADING_GALLERY = 'SET_LOADING_GALLERY';
export const GET_GALLERY_SUCCESS = 'GET_GALLERY_SUCCESS';

interface getGallery {
    type: typeof GET_GALLERY,
    topic: string,
    perPage: number,
}

interface setLoadingGallery {
    type: typeof SET_LOADING_GALLERY,
    isLoading: boolean
}

interface getGallerySuccess {
    type: typeof GET_GALLERY_SUCCESS
    photos: Array<IPhoto>
}

export type galleryActionType = getGallery | getGallerySuccess | setLoadingGallery;