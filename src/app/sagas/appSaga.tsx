import { all } from 'redux-saga/effects';
import { gallerySaga } from './gallerySaga';
//import some generate func

export default function* rootSaga() {
  yield all([
    gallerySaga()
  ]);
}
