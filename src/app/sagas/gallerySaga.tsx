import { all, takeLatest, put, call } from "redux-saga/effects";
import { AxiosResponse } from "axios";
import { galleryServices } from "../services/galleryServices";
import { getGallerySuccess, setLoadingGallery } from "../actions/galleryActions";
import { galleryActionType, GET_GALLERY } from "../actionTypes/galleryActionTypes";

function* getGallerySaga(action: any) {
  yield put(setLoadingGallery(true));
  const { data }: AxiosResponse = yield call(galleryServices.getGallery, action.topic, action.perPage);
  console.log('getGallerySaga', data);
  yield put(getGallerySuccess(data.photos));
}

function* watchGetGallery() {
  yield takeLatest(GET_GALLERY, getGallerySaga);
}

export function* gallerySaga() {
  yield all([
    watchGetGallery()
  ]);
}
