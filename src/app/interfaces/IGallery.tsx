export interface IAppState {
  galleryState: IGalleryState
}

export interface IGalleryState {
  isLoading: boolean;
  showedPhotos: Array<IPhoto>,
  allPhotos: Array<IPhoto>,
  peoplePhotos: Array<IPhoto>,
  placesPhotos: Array<IPhoto>,
  thingsPhotos: Array<IPhoto>,
}

export interface IPhoto {
  width: number;
  height: number;
  url: string;
  src: {
    landscape: string;
    large: string;
    large2x: string;
    medium: string;
    original: string;
    portrait: string;
    small: string;
    tiny: string;
  }
}