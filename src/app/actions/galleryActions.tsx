import { galleryActionType, GET_GALLERY, GET_GALLERY_SUCCESS, SET_LOADING_GALLERY } from "../actionTypes/galleryActionTypes";
import { IPhoto } from "../interfaces/IGallery";

export const getGallery = (topic: string, perPage: number): galleryActionType => ({
  type: GET_GALLERY,
  topic,
  perPage
})

export const getGallerySuccess = (photos: Array<IPhoto>): galleryActionType => ({
  type: GET_GALLERY_SUCCESS,
  photos
})

export const setLoadingGallery = (isLoading: boolean): galleryActionType => ({
  type: SET_LOADING_GALLERY,
  isLoading
})