import axios from "axios";

const CancelToken = axios.CancelToken;
export const sourceCancel = CancelToken.source();

const instanceAppAxios = axios.create({
    baseURL: "https://api.pexels.com/",
    cancelToken: sourceCancel.token,
});

instanceAppAxios.defaults.headers.common['Authorization'] = '563492ad6f91700001000001e0f76c371fb44c01b0ba6eb386b524b2';

// Add a response interceptor
instanceAppAxios.interceptors.response.use(
    function (response) {
        // Do something with response data
        return response;
    },
    function (error) {
        // Handle when not Auth
        if (error.response && +error.response.status === 401) {
            alert("Tài khoản hết hạn, xin đăng nhập lại!");
            window.location.href = "/login";
            return;
        }

        // Handle when not Auth
        if (error.response && +error.response.status === 400) {
            alert("Có gì đó sai, hãy thử lại!");
            window.location.href = "/login";
            return;
        }

        // Handle server error
        if (error.response && +error.response.status === 500) {
            alert("Máy chủ gặp lỗi, vui lòng thử lại sau!");
            window.location.href = "/";
            return;
        }
        return Promise.reject(error);
    }
);

export { instanceAppAxios as appAxios };