import React, { useState } from "react";
import HomeContainer from "src/components/Home";
import "src/styles/_all.scss";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import GalleryContainer from "src/components/Gallery";
import GeneicContainer from "src/components/Generic";


interface Props {

}

const App: React.FC<Props> = (props) => {
  const [path, setPath] = useState('/');


  return (
    <Router>
      <div className="page-wrap">

        {/* <!-- Nav --> */}
        <nav id="nav">
          <ul>
            <li><Link onClick={() => setPath("/")} to="/" className={path === '/' ? "active" : ''}><span className="icon fa-home"></span></Link></li>
            <li><Link onClick={() => setPath("/gallery")} to="/gallery" className={path === '/gallery' ? "active" : ''}><span className="icon fa-camera-retro"></span></Link></li>
            <li><Link onClick={() => setPath("/generic")} to="/generic" className={path === '/generic' ? "active" : ''}><span className="icon fa-file-text-o"></span></Link></li>
          </ul>
        </nav>

        {/* <!-- Main --> */}
        <section id="main">
          {/* <!-- Gallery --> */}
          <Switch>
            <Route exact path="/">
              <HomeContainer />
            </Route>
            <Route exact path="/gallery">
              <GalleryContainer />
            </Route>
            <Route exact path="/generic">
              <GeneicContainer />
            </Route>
          </Switch>

          {/* <!-- Contact --> */}
          <section id="contact">
            {/* <!-- Social --> */}
            <div className="social column">
              <h3>About Me</h3>
              <p>Mus sed interdum nunc dictum rutrum scelerisque erat a parturient condimentum potenti dapibus vestibulum
              condimentum per tristique porta. Torquent a ut consectetur a vel ullamcorper a commodo a mattis ipsum
            class quam sed eros vestibulum quisque a eu nulla scelerisque a elementum vestibulum.</p>
              <p>Aliquet dolor ultricies sem rhoncus dolor ullamcorper pharetra dis condimentum ullamcorper rutrum
              vehicula id nisi vel aptent orci litora hendrerit penatibus erat ad sit. In a semper velit eleifend a
              viverra adipiscing a phasellus urna praesent parturient integer ultrices montes parturient suscipit
              posuere quis aenean. Parturient euismod ultricies commodo arcu elementum suspendisse id dictumst at ut
              vestibulum conubia quisque a himenaeos dictum proin dis purus integer mollis parturient eros scelerisque
            dis libero parturient magnis.</p>
              <h3>Follow Me</h3>
              <ul className="icons">
                <li><a href="#" className="icon fa-twitter"><span className="label">Twitter</span></a></li>
                <li><a href="#" className="icon fa-facebook"><span className="label">Facebook</span></a></li>
                <li><a href="#" className="icon fa-instagram"><span className="label">Instagram</span></a></li>
              </ul>
            </div>

            {/* <!-- Form --> */}
            <div className="column">
              <h3>Get in Touch</h3>
              <form action="#" method="post">
                <div className="field half first">
                  <label htmlFor="name">Name</label>
                  <input name="name" id="name" type="text" placeholder="Name" />
                </div>
                <div className="field half">
                  <label htmlFor="email">Email</label>
                  <input name="email" id="email" type="email" placeholder="Email" />
                </div>
                <div className="field">
                  <label htmlFor="message">Message</label>
                  <textarea name="message" id="message" rows={6} placeholder="Message"></textarea>
                </div>
                <ul className="actions">
                  <li><input value="Send Message" className="button" type="submit" /></li>
                </ul>
              </form>
            </div>

          </section>

          {/* <!-- Footer --> */}
          <footer id="footer">
            <div className="copyright">
              &copy; Untitled Design: <a href="https://templated.co/">TEMPLATED</a>. Images: <a
                href="https://unsplash.com/">Unsplash</a>.
        </div>
          </footer>
        </section>
      </div>
    </Router>
  );
};

export default App;
