import React from 'react';

export const Loading = () => {
    return (
        <div className="loading-ctn">
            <img src="images/loading.gif" width="200px" height="200px" alt="" />
        </div>
    )
}