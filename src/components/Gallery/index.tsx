import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getGallery } from 'src/app/actions/galleryActions';
import { IAppState, IPhoto } from 'src/app/interfaces/IGallery';
import { Loading } from '../commons/Loading';

interface Props {

}

const GalleryContainer: React.FC<Props> = () => {
  const { showedPhotos, isLoading } = useSelector((state: IAppState) => state.galleryState);
  const [topic, setTopic] = React.useState('baby');
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(getGallery(topic, 16));
  }, [topic]);

  console.log(showedPhotos);
  return (
    <section id="main">

      {/* <!-- Header --> */}
      <header id="header">
        <div>Snapshot <span>by TEMPLATED</span></div>
      </header>

      {/* <!-- Gallery --> */}
      <section id="galleries">

        {/* <!-- Photo Galleries --> */}
        <div className="gallery">

          {/* <!-- Filters --> */}
          <header>
            <h1>Gallery</h1>
            <ul className="tabs">
              <li><a href="#" onClick={() => setTopic('baby')} className={`button ${topic === 'baby' && 'active'}`}>Baby</a></li>
              <li><a href="#" onClick={() => setTopic('people')} className={`button ${topic === 'people' && 'active'}`}>People</a></li>
              <li><a href="#" onClick={() => setTopic('places')} className={`button ${topic === 'place' && 'active'}`}>Places</a></li>
              <li><a href="#" onClick={() => setTopic('things')} className={`button ${topic === 'things' && 'active'}`}>Things</a></li>
            </ul>
          </header>
          {isLoading ? <Loading /> :
          <div className="content">
            {
              showedPhotos.map((photo: IPhoto, idx: number) => (
                <div key={idx} className="media all people">
                  <img className="img" src={photo.src.large} alt="" title="This right here is a caption." />
                </div>
              ))
            }
          </div>
          }
        </div>
      </section>
    </section>
  )
}

export default GalleryContainer;