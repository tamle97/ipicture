import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getGallery } from 'src/app/actions/galleryActions';
import { IAppState, IPhoto } from 'src/app/interfaces/IGallery';

interface Props {

}

const HomeContainer: React.FC<Props> = () => {
  const { showedPhotos } = useSelector((state: IAppState) => state.galleryState);

  const dispatch = useDispatch();
  React.useEffect(() => {
    if (!showedPhotos.length) {
      dispatch(getGallery('people', 16));
    }
  }, []);

  return (
    <section id="main">
      {/* <!-- Banner --> */}
      <section id="banner">
        <div className="inner">
          <h1>Hey, I'm Snapshot</h1>
          <p>A fully responsive gallery template by <a href="https://templated.co">TEMPLATED</a></p>
          <ul className="actions">
            <li><a href="#galleries" className="button alt scrolly big">Continue</a></li>
          </ul>
        </div>
      </section>
      {/* <!-- Gallery --> */}
      <section id="galleries">

        {/* <!-- Photo Galleries --> */}
        <div className="gallery">
          <header className="special">
            <h2>What's New</h2>
          </header>
          <div className="content">
            {
              showedPhotos.slice(0, 8).map((photo: IPhoto, idx: number) => (
                <div key={idx} className="media all people">
                  <img className="img" src={photo.src.large} alt="" title="This right here is a caption." />
                </div>
              ))
            }
          </div>
          <footer>
            <a href="/gallery" className="button big">Full Gallery</a>
          </footer>
        </div>
      </section>
    </section>
  )
}

export default HomeContainer;